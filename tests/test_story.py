"""
Test module for Story model
"""
import pytest

from model import Story
from settings import SHORT_DESCRIPTION_PROBLEM, LONG_DESCRIPTION_PROBLEM, NO_AC_PROBLEM, \
    TO_MANY_AC_PROBLEM, NO_AS_A_PROBLEM, I_WOULD_PROBLEM, NO_SO_THAT_PROBLEM


@pytest.mark.parametrize(
    "name,description",
    [
        (1, 1),
        ("test", 1),
        ("test", "test")
    ]
)
def test_story_object_creation(name, description):
    """
    Function to test story object creation
    :param name: test story name
    :param description: test story description
    """
    try:
        test_story = Story(name, description)
        assert test_story
    except TypeError as type_exc:
        assert 'name' in type_exc.message or 'description' in type_exc.message


@pytest.mark.parametrize(
    "name,description",
    [
        ("Test short story", "Test short description"),
        ("Test long story", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                            "Aliquam faucibus porta dui, non auctor mauris varius id. "
                            "Class aptent taciti sociosqu ad litora torquent per conubia "
                            "nostra, per inceptos himenaeos. Ut nec finibus odio. "
                            "In placerat, orci vitae aliquet blandit, leo eros sagittis "
                            "odio, luctus pulvinar urna eros sit amet ipsum. "
                            "Quisque mauris diam, fermentum ut ipsum at, auctor laoreet "
                            "sapien. Donec eleifend et nulla condimentum aliquet. Donec "
                            "mattis lacus et nulla lobortis, eu ultrices dolor ultricies. "
                            "Proin eu augue et mi varius consectetur. Proin velit mi, "
                            "interdum vestibulum elementum id, cursus et risus. "
                            "Phasellus vitae congue lorem. Sed neque erat, imperdiet eget "
                            "odio eget, semper mollis justo. Phasellus facilisis vestibulum "
                            "magna eu pharetra. Donec consequat aliquam nisi quis maximus. "
                            "Nulla congue odio sed imperdiet vulputate. Cras ultrices diam "
                            "sed sem maximus eleifend. Nulla interdum tristique placerat. "
                            "Duis ac maximus velit. Curabitur leo ligula, interdum nec "
                            "lectus vitae, aliquam tristique turpis. Phasellus at eros "
                            "luctus, egestas nunc et, scelerisque libero. Nullam vel nulla "
                            "tempus, commodo neque a, tincidunt purus. Morbi diam ipsum, "
                            "varius et risus nec, rutrum pharetra libero. Aenean tempor "
                            "sit amet lectus sit amet pellentesque. Nulla a augue condimentum, "
                            "consectetur erat ac, iaculis tortor. Nulla luctus facilisis orci.")
    ]
)
def test_story_description(name, description):
    """
    Function to test story description
    :param name: test story name
    :param description: test story description
    """
    test_story = Story(name, description)

    story_description_problems = test_story.analyze_description_length()

    assert SHORT_DESCRIPTION_PROBLEM in story_description_problems \
           or LONG_DESCRIPTION_PROBLEM in story_description_problems


@pytest.mark.parametrize(
    "name,description",
    [
        (
            "Test Story without AC in description",
            "Story description without any acceptance"
            "criteria in this short block of text"
        ),
        (
            "Test Story with to many AC in description",
            "AC 1: First acceptance criteria"
            "AC 2: Second acceptance criteria"
            "AC 3: Third acceptance criteria"
            "AC 4: Fourth acceptance criteria"
            "AC 5: Fifth acceptance criteria"
            "AC 6: Sixth acceptance criteria"
            "AC 7: Seventh acceptance criteria"
            "AC 8: Eighth acceptance criteria"
            "AC 9: Ninth acceptance criteria"
            "AC 10: Tenth acceptance criteria"
            "AC 11: Eleven acceptance criteria"
        )
    ]
)
def test_story_acceptance_criteria(name, description):
    """
    Function to test story acceptance criteria
    :param name: test story name
    :param description: test story description
    """
    test_story = Story(name, description)

    story_ac_problems = test_story.analyze_ac_number()

    assert NO_AC_PROBLEM in story_ac_problems or TO_MANY_AC_PROBLEM in story_ac_problems


@pytest.mark.parametrize(
    "name,description",
    [
        ("Wrong test story name", "Some short description")
    ]
)
def test_story_name(name, description):
    """
    Function to test story name
    :param name: test story name
    :param description: test story description
    """
    test_story = Story(name, description)

    story_name_problems = test_story.analyze_story_name()

    assert NO_AS_A_PROBLEM in story_name_problems \
           or I_WOULD_PROBLEM in story_name_problems \
           or NO_SO_THAT_PROBLEM in story_name_problems


@pytest.mark.parametrize(
    "name,description",
    [
        (
            "As an internet banking customer I want to see a rolling balance for my everyday"
            "account so that I know the balance of my account after each transaction is applied",
            "AC 1: The rolling balance is displayed,"
            "AC 2: The rolling balance is calculated for each transaction,"
            "AC 3: The balance is displayed for every transaction for the full period of time "
            "transactions ara available"
            "AC 4: The balance is not displayed if a filter has been applied"
        )
    ]
)
def test_correct_story_creation(name, description):
    """
    Function to test correct story data
    :param name: test story name
    :param description: test story description
    """
    test_story = Story(name, description)

    story_problems = test_story.analyze_quality()

    assert not story_problems
