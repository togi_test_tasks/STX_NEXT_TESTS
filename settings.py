"""
This module contains default values for project
"""
from envparse import env


env.read_envfile()

MIN_STORY_WORD_COUNT = env.int('MIN_STORY_WORD_COUNT', default=10)
MAX_STORY_WORD_COUNT = env.int('MAX_STORY_WORD_COUNT', default=200)

MAX_STORY_AC = env.int('MAX_STORY_AC', default=10)

SPLIT_WORD_PATTERN = env.str('SPLIT_WORD_PATTERN', default=r'\W+')
AC_CRITERIA_PATTERN = env.str('AC_CRITERIA_PATTERN', default=r'AC\s*\d+')
AS_A_NAME_PATTERN = env.str('AS_A_NAME_PATTERN', default=r'^as a')
I_WOULD_NAME_PATTERN = env.str('I_WOULD_NAME_PATTERN', default=r'i would like|i want')
SO_THAT_NAME_PATTERN = env.str('SO_THAT_NAME_PATTERN', default=r'so that')

SHORT_DESCRIPTION_PROBLEM = env.str('SHORT_DESCRIPTION_PROBLEM', default='short-description')
LONG_DESCRIPTION_PROBLEM = env.str('LONG_DESCRIPTION_PROBLEM', default='long-description')
NO_AC_PROBLEM = env.str('NO_AC_PROBLEM', default='no-acs')
TO_MANY_AC_PROBLEM = env.str('TO_MANY_AC_PROBLEM', default='too-many-acs')
NO_AS_A_PROBLEM = env.str('NO_AS_A_PROBLEM', default='no-as-a')
I_WOULD_PROBLEM = env.str('I_WOULD_PROBLEM', default='no-i-want')
NO_SO_THAT_PROBLEM = env.str('NO_SO_THAT_PROBLEM', default='no-so-that')
