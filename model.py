"""
This module contains fixed version with models definition to work and pass pylint code check
"""

import re

from settings import MIN_STORY_WORD_COUNT, MAX_STORY_WORD_COUNT, MAX_STORY_AC, SPLIT_WORD_PATTERN, \
    AC_CRITERIA_PATTERN, AS_A_NAME_PATTERN, I_WOULD_NAME_PATTERN, SO_THAT_NAME_PATTERN, \
    SHORT_DESCRIPTION_PROBLEM, LONG_DESCRIPTION_PROBLEM, NO_AC_PROBLEM, TO_MANY_AC_PROBLEM, \
    NO_AS_A_PROBLEM, I_WOULD_PROBLEM, NO_SO_THAT_PROBLEM


class Story(object):
    """
    Scrum story model definition
    """
    def __init__(self, name, description, assignee=None):
        to_check = (
            ('name', name),
            ('description', description),
        )
        for check_name, value in to_check:
            if not isinstance(value, str):
                raise TypeError('%s must be a string' % check_name)

        self.name = name
        self.description = description
        self.assignee = assignee

    def __str__(self):
        return 'Story: {}'.format(self.name)

    def analyze_description_length(self):
        """
        Method to analyze story description length problems
        :return: set with found description problems
        """
        description_problems = set()

        split_word = re.compile(SPLIT_WORD_PATTERN)
        description_words = split_word.split(self.description)

        description_word_count = len(description_words)

        if description_word_count < MIN_STORY_WORD_COUNT:
            description_problems.add(SHORT_DESCRIPTION_PROBLEM)
        elif description_word_count > MAX_STORY_WORD_COUNT:
            description_problems.add(LONG_DESCRIPTION_PROBLEM)

        return description_problems

    def analyze_ac_number(self):
        """
        Method to analyze story acceptance criteria problems
        :return: set with found acceptance criteria problems
        """
        ac_problems = set()

        ac_criteria = re.compile(AC_CRITERIA_PATTERN)

        acceptance_criteria = ac_criteria.findall(self.description)
        if not acceptance_criteria:
            ac_problems.add(NO_AC_PROBLEM)
        elif len(acceptance_criteria) > MAX_STORY_AC:
            ac_problems.add(TO_MANY_AC_PROBLEM)

        return ac_problems

    def analyze_story_name(self):
        """
        Method to analyze story likeness of name problems
        :return: set with found story likeness of name problems
        """
        story_problems = set()

        as_a = re.compile(AS_A_NAME_PATTERN, re.I | re.M)
        i_would = re.compile(I_WOULD_NAME_PATTERN, re.I | re.M)
        so_that = re.compile(SO_THAT_NAME_PATTERN, re.I | re.M)

        if not as_a.findall(self.name):
            story_problems.add(NO_AS_A_PROBLEM)
        if not i_would.findall(self.name):
            story_problems.add(I_WOULD_PROBLEM)
        if not so_that.findall(self.name):
            story_problems.add(NO_SO_THAT_PROBLEM)

        return story_problems

    def analyze_quality(self):
        """
        Method to analyze story problems
        :return: set with found story problems
        """
        problems = set()

        problems.update(self.analyze_description_length())
        problems.update(self.analyze_ac_number())
        problems.update(self.analyze_story_name())

        return problems


class Chore(Story):
    """
    Scrum chore model definition
    """
    def __init__(self, name, description, assignee=None):
        super(Chore, self).__init__(name, description)

    def analyze_quality(self):
        """
        Method to analyze chore problems
        :return: set with found chore problems
        """
        problems = set()

        problems.update(self.analyze_description_length())
        problems.update(self.analyze_ac_number())

        return problems
